from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"msg": "Hello World"}

@app.get("/tree/{tree_name}")
async def my_tree(tree_name: str):
    return {"myFavouriteTree": tree_name}