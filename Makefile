.PHONY: test build kubernetes

test:
	pip install --user pipenv
	pipenv run pytest src/app_test.py

build:
	docker build -t $(name)/ecosia:latest .

kubernetes:
	kubectl apply -f kube/deployment.yml
	kubectl apply -f kube/ingress.yml
	kubectl apply -f kube/namespace.yml

	echo "$(minikube ip) local.ecosia.org" | sudo tee -a /etc/hosts